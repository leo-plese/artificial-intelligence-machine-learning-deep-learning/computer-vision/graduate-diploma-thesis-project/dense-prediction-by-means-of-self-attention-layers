# Dense Prediction by Means of Self-attention Layers

Dense Prediction by Means of Self-attention Layers - research of models for dense prediction (semantic segmentation) - primarily transformers (models in focus: Segmenter, Swin transformer) and comparison with convolutional models (model in focus: pyramidal SwiftNet). Also, research, design and implementation of pyramidal models of transformer-convolutional model (Segmenter-SwiftNet) and transformer-transformer (Segmenter-Segmenter) type. Implementation is in PyTorch deep learning framework.

My graduate thesis computer vision (deep learning, machine learning, artificial intelligence) project.

Research paper and presentation are available in this repository.
Implementations of the models (adjusted official implementations and hybrid architectures I implemented) are in the repositories in this project group named "Graduate Diploma Thesis Project":

-"Segmenter Transformer Models for Semantic Segmentation" - Segmenter (https://gitlab.com/artificial-intelligence-machine-learning-deep-learning/computer-vision/graduate-diploma-thesis-project/segmenter-transformer-models-for-semantic-segmentation)

-"Swin Transformer Models for Semantic Segmentation" - Swin Transformer (https://gitlab.com/artificial-intelligence-machine-learning-deep-learning/computer-vision/graduate-diploma-thesis-project/swin-transformer-models-for-semantic-segmentation)

-"SwiftNet Convolutional Models with Pyramidal Fusion for Semantic Segmentation" - SwiftNet (https://gitlab.com/artificial-intelligence-machine-learning-deep-learning/computer-vision/graduate-diploma-thesis-project/swiftnet-convolutional-models-with-pyramidal-fusion-for-semantic-segmentation)

-"SwiftNet-Segmenter - NEW Convolutional-Transformer Architecture - Ensembles and Joint Model Training" - ensembles and joint models of Segmenter transformer and pyramidal SwiftNet convolutional model (https://gitlab.com/artificial-intelligence-machine-learning-deep-learning/computer-vision/graduate-diploma-thesis-project/swiftnet-segmenter-new-convolutional-transformer-architecture-ensembles-and-joint-model-training)

Project duration: Feb 2022 - July 2022.
